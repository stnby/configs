# configs

## This is a collection of configurations for Nixos/GuixSD for starters to look at.  

Contributor | Device | OS
--- | --- | ---
Laalf | W530 | NixOS
chonms | X1 Carbon 6 | NixOS
Levy | X220/E2140/MB4,1 | GuixSD

