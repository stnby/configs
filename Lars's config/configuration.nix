{ config, pkgs, lib, ... }:
{
  /*
  # For initial setup you need to install git. nix SOMEHOW STILL doesn't pull git in if it needs it. 
  # To install with encrypted root i use:
  nix-channel --update && nix-env -iA nixos.git #install git
  wipefs -a /dev/sda
  cfdisk /dev/sda #partition the drive here
  # 1 500M /boot partition. Type EFI
  # Rest type Linux
  echo -n "your_password" | cryptsetup luksFormat /dev/sda2 - #password should be identical to other drives you want unlocked at boot
  echo -n "your_password" | cryptsetup luksOpen /dev/sda2 enc-pv -
  pvcreate /dev/mapper/enc-pv
  vgcreate vg /dev/mapper/enc-pv
  lvcreate -L 8G -n swap vg #8G swap should be more than enough. We have earlyoom.
  lvcreate -l '100%FREE' -n root vg
  mkfs.fat /dev/sda1 #efi
  mkfs.ext4 -L root /dev/vg/root #i want dedup and compression for filesystems like this already
  mkswap -L swap /dev/vg/swap
  mount /dev/vg/root /mnt
  mkdir /mnt/boot
  mount /dev/sda1 /mnt/boot
  swapon /dev/vg/swap
  
  # To set up lvm caching you use this: 
  # Delete partition tables beforehand
  wipefs -a /dev/sdb
  wipefs -a /dev/sdc
  
  # Use the same password for all drives for auto-unlocking
  echo -n "your_password" | cryptsetup luksFormat /dev/sdb - #my hdd
  echo -n "your_password" | cryptsetup luksOpen /dev/sdb mass -
  echo -n "your_password" | cryptsetup luksFormat /dev/sdc - #my ssd, for cache
  echo -n "your_password" | cryptsetup luksOpen /dev/sdc cache -
  
  pvcreate /dev/mapper/mass
  vgcreate vg0 /dev/mapper/mass
  lvcreate -l 100%pvs -n datalv vg0 /dev/mapper/mass
  
  pvcreate /dev/mapper/cache
  vgextend vg0 /dev/mapper/cache
  lvcreate --type cache-pool -l 100%pvs -n cache vg0 /dev/mapper/cache
  lvconvert --type cache --cachepool vg0/cache vg0/datalv
  
  pvscan
  vgscan
  lvscan
  vgchange -ay
  mkfs.ext4 /dev/mapper/vg0-datalv
  mkdir -p /mnt/home/lars/Videos
  # edit the service instead of mounting it. nixos can't boot if you put the cached setup in the fstab
  */
  imports =
    [
    /etc/nixos/hardware-configuration.nix
    (import (builtins.fetchGit { url = "https://github.com/rycee/home-manager.git"; ref = "master";}) {}).nixos
  ];
  nixpkgs.config.packageOverrides = pkgs: { 
      comptonkawase = pkgs.compton.overrideAttrs (oldAttrs: { # i dont use a windowmanager anymore
        src = pkgs.fetchFromGitLab{
          owner = "dorian7k7";
          repo = "compton-kawase-blur";
          rev = "4b9ac1e83fd783ea2dfa68dfc5f00da99722e12d";
          sha256 = "0xc2hmhpa6zbw09xmhwacqhlgqfdc0g6d4b06r5k56c3ngafsyp0";
          };
      });
      chromiumVAAPI = pkgs.chromium.override { #is that option included yet?
        VAAPISupport = true;
      };
  };
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true; #only mumble needs that
  nixpkgs.config.android_sdk.accept_license = true; #why the fuck is that a config?

  boot = {
    tmpOnTmpfs = true; #tmp should be a tmpfs. thanks
    initrd.kernelModules = [ "nouveau" "i915" "vfio-pci" ]; #lets try to not break sddm or libvirt
    initrd.luks.devices = [ #cuz encryption is fast nowadays
      {
        name = "root";
        device = "/dev/disk/by-uuid/a24d974c-6281-49f9-9b4b-9bba76808813"; #/dev/sda2
        preLVM = true;
        allowDiscards = true;
      }
      {#password can get reused. 
        name = "videos";
        device = "/dev/disk/by-uuid/3ee0caa8-eb7f-4354-ad41-97a5930373d9"; #/dev/sdb
        preLVM = true;
      }
      {
        name = "cache";
        device = "/dev/disk/by-uuid/b7799a8f-f7e4-4d9f-9dac-c73d2fd00012"; #/dev/sdc
        preLVM = true;
      }
    ];
    plymouth.enable = true; #intel fucks it up anyways, why do i do that?
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = [ "ntfs" "btrfs" ]; #idk, i can write to ntfs now
    extraModprobeConfig =  ''
      options thinkpad_acpi fan_control=1
      options thinkpad_acpi experimental=1
      options i915 fastboot=1
      options i915 enable_fbc=1
      options i915 lvds_downclock=1
      options i915 modeset=0
    '';
    loader = {
      grub = {
        enable = true;
        version = 2;
        device = "nodev";
        efiSupport = true;
        efiInstallAsRemovable = true; #lets make our system bootable after a BIOS reset
      };
    };
    kernelModules = [ "acpi_call" "tp-smapi" "kvm-intel" "dm-thin-pool" "dm_cache" "dm_persistent_data" "dm_bio_prison" "dm_bufio" "libcrc32c" "crc32c_generic" "dm_cache_smq"]; #thanks novideo
    extraModulePackages = [
      config.boot.kernelPackages.tp_smapi 
      config.boot.kernelPackages.acpi_call];
      kernelParams = [ "intel_iommu=on" "net.ifnames=0" "biosdevname=0" "acpi_osi=\"!Windows 2012\"" "nmi_watchdog=0" "snd-hda-intel.power_save=1" "iwlwifi.led_mode=2" "iwlwifi.power_save=Y" "iwlwifi.11n_disable=1" "e1000e.SmartPowerDownEnable=1" "quiet" "vga=current"]; #make it stop
  };
  virtualisation = {
    libvirtd =  {
      enable = true;
      extraConfig = ''
            unix_sock_group = "libvirtd"
            nix_sock_rw_perms = "0770"''; #why is that not default. is it default? don't care
      qemuOvmf = true;
      qemuVerbatimConfig = ''user = "1000" ''; #audio
    };
  };
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "de"; #reee bratwurst
    defaultLocale = "en_US.UTF-8";
  };
  time.timeZone = "Europe/Berlin";
  environment.systemPackages = with pkgs;  [
    gnome3.dconf #virtmanager shits itself otherwise
    acpi
    tpacpi-bat #when was the last time i recalibrated my battery?
    tlp
    zsh-completions
    nix-zsh-completions #still wondering if they actually work...
    mumble_git
    libsForQt511.qtstyleplugin-kvantum #BLUR
  ];
  networking = {
    hostName = "NixLarf";
    networkmanager.enable = true;
    firewall.enable = false;
  };
  hardware = {
    cpu.intel.updateMicrocode = true;#super fast botnet upgrades
    opengl = {
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntel #accelerated video is a mystery
        libva-full
        ];
    };
    pulseaudio.enable = true; #hewwo lennart!
    pulseaudio.support32Bit = true;
    bluetooth.enable = true;
  };
  sound.enable = true; # why alsa again?
  sound.mediaKeys.enable = true;
  services = {
    tlp = {
      enable = true;
         extraConfig = ''
            SATA_LINKPWR_ON_AC=max_performance
            SATA_LINKPWR_ON_BAT=min_power
            WIFI_PWR_ON_AC=off
            WIFI_PWR_ON_BAT=on
            CPU_SCALING_GOVERNOR_ON_AC=powersave
            CPU_SCALING_GOVERNOR_ON_BAT=powersave
            DEVICES_TO_ENABLE_ON_STARTUP="wifi"
            DEVICES_TO_DISABLE_ON_STARTUP="bluetooth wwan"
            RESTORE_DEVICE_STATE_ON_STARTUP=0
            RESTORE_THRESHOLDS_ON_BAT=0
            DISK_DEVICES="ata-ST2000LM015-2E8174_WDZ4QM43"
            DISK_APM_LEVEL_ON_AC="192"
            DISK_APM_LEVEL_ON_BAT="1"
            DISK_SPINDOWN_TIMEOUT_ON_AC="36"
            DISK_SPINDOWN_TIMEOUT_ON_BAT="18"
            USB_AUTOSUSPEND=1
        '';
      };
    earlyoom = {
      enable = true;
      freeMemThreshold = 2;
    };
    flatpak.enable = true; #AH MAKE IT STOP
    fstrim.enable = true; #i should visit the barber some time
    thermald.enable = true; #liquid metal is not enough
    printing.enable = true; #i don't even own a printer
    colord.enable = true; #thats gay
    samba.enable = true; #dancin. i don't even have Windows running here
    xserver = {
      enable =true;
      layout = "de";
      libinput.enable = true; #why is that an extra option? 
       displayManager.sessionCommands = '' 
        xrandr --setprovideroutputsource 1 0
        xmodmap /home/lars/.Xmodmap
        ln -sf /home/lars/.nix-profile/share/applications/org.keepassxc.KeePassXC.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/flameshot.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/nextcloud.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/telegram-desktop.desktop /home/lars/.config/autostart/.
        ''; #the "aut" in autostart stands for autistic. also fuck you nvidia. The xrandr command makes displays connected to the nvidia (on my W530 all external ones) available to output to.
      displayManager.sddm.enable = true;
      desktopManager = {
        xterm.enable = false;
        plasma5 = { #show me a decent alternative, kthxbye
          enable = true;
          enableQt4Support = true; #Who uses qt4 anymore?
        };
      };
    };
  };
  systemd.services.guix-daemon = { #Guix daemon for extra python compiling
    enable = true;
    description = "Build daemon for GNU Guix";
    serviceConfig = {
      ExecStart = "/var/guix/profiles/per-user/root/guix-profile/bin/guix-daemon --build-users-group=guixbuild";
      Environment="GUIX_LOCPATH=/root/.guix-profile/lib/locale";
      RemainAfterExit="yes";
      StandardOutput="syslog";
      StandardError="syslog";
      TaskMax= "8192";
    };
    wantedBy = [ "multi-user.target" ];
  };
  systemd.services.videomount = { #lvm caching breaks nixos. leaving out cached mount in hardware-configuration, else you can't start.
    enable = true;
    description = "Mounts Videos";
    serviceConfig = {
      ExecStartPre = "${pkgs.lvm2}/bin/pvscan ; ${pkgs.lvm2}/bin/vgscan ; ${pkgs.lvm2}/bin/lvscan ; ${pkgs.lvm2}/bin/vgchange -ay";
      ExecStart = "${pkgs.utillinux}/bin/mount /dev/mapper/vg0-datalv /home/lars/Videos";
    };
    wantedBy = [ "multi-user.target" ];
  };
  systemd.services.autoGc = {
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-collect-garbage --delete-older-than 14d";
    };
  };
  systemd.timers.autoGc = { #lets clean up and auto-hardlink at 2 am
    enable = true;
    timerConfig = {
      Unit = "autoGc.service";
      OnCalendar = "02:00";
      Persistent = "true";
    };
    wantedBy = [ "timers.target" ];
  };
  systemd.services.autoUpgrade = {
    after = [ "network-online.target" "autoGc.service" ];
    path = [ pkgs.gnutar pkgs.xz.bin config.nix.package.out ];
    environment = config.nix.envVars //
    { inherit (config.environment.sessionVariables) NIX_PATH;
    HOME = "/root";
  } // config.networking.proxy.envVars; #nix sucks

    serviceConfig = {
      ExecStart = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch --upgrade";
    };
    wantedBy= [ "autoGc.service" "autoUpgrade.service" ];
  };
  systemd.services.autoOptimise = {
    after = [ "network-online.target" ];
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-store --optimise";
    };
    wantedBy = [ "autoUpgrade.service" ];
  };
  systemd.extraConfig = "DefaultLimitNOFILE=1048576"; #thanks lennart, please stop crashing my system because i have too many open files
  programs.adb.enable = true;
  users.extraUsers = {
    lars = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/lars";
      extraGroups = [ "wheel" "networkmanager" "libvirtd" "video" "adbusers" "kvm" ];
      shell = pkgs.zsh;
    };
    guixbuilder1 = { #i will write a function for that... next year
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder2 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder3 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder4 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder5 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder6 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder7 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder8 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    };
  environment.pathsToLink = [ "/share/zsh" ]; #systemd-autocomplete uwu
  nix.nixPath = ["nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos" "nixos-config=/home/lars/configs/Lars\'s\ config/configuration.nix" "/nix/var/nix/profiles/per-user/root/channels" ]; #i moved my config to my home, cool hm?
  home-manager.users.lars =
    let
      master = import (builtins.fetchGit { url = https://github.com/NixOS/nixpkgs.git; ref = "master";}) {};#this is kinda cool, hm?
      stable = import (builtins.fetchGit { url = https://github.com/NixOS/nixpkgs.git; ref = "release-18.09";}) {};#sucks that unstable contains broken packages...
    in {
      programs.zsh.enable = true;
      programs.zsh.history.size = 1000000;
      programs.zsh.oh-my-zsh.enable = true; #i kinda want zim zsh here but i am too lazy to do that
      programs.zsh.oh-my-zsh.plugins = ["history" "git" "cp" "systemd" "colorize" "command-not-found"];
      programs.zsh.plugins = [
        {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-autosuggestions";
          rev = "v0.5.0";
          sha256 = "19qkg4b2flvnp2l0cbkl4qbrnl8d3lym2mmh1mx9nmjd7b81r3pf";
          };
        }
        {
        name = "zsh-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-syntax-highlighting";
          rev = "v0.6.0";
          sha256 = "0zmq66dzasmr5pwribyh4kbkk23jxbpdw4rjxx0i7dx8jjp2lzl4";
          };
        }
      ];
      programs.zsh.oh-my-zsh.theme = "agnoster";#first one that is not completely ugly
      programs.zsh.shellAliases = {
        nup = "env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
        nupc = "sudo nix-collect-garbage -d && sudo nix-store --optimise && env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
      }; # what the fuck is that? nix-store --optimise takes a long time on large stores. Though hardlinking can improve a lot sometimes... might as well use it "sometimes"
      programs.zsh.initExtra = "zstyle \":completion:*:commands\" rehash 1 
      source ~/.profile"; #autorehash and sourcing .profile makes sense
      nixpkgs.config.allowUnfree = true; #forgive me stallman for i have sinned. x11vnc sucks though
      home.file.".Xmodmap".text = ''
          remove Lock = Caps_Lock
          keysym Caps_Lock = Escape
          '';#who needs caps
      home.packages = with pkgs; [
        master.tdesktop #i wanna compile telegram
        stable.dwarf-fortress-packages.dwarf-fortress-full
        stable.freedroidrpg
        stable.stuntrally
        stable.virtmanager
        stable.vdrift #i didn't test any of those games extensively yet
        anydesk
        arc-kde-theme
        ark
        chromium
        clementine #i don't even use it
        cmatrix
        evince
        ffmpeg-full
        flameshot #is gud
        freerdp #or this
        gimp-with-plugins
        git
        glibcLocales
        glxinfo
        htop
        jdk
        kdeApplications.gwenview
        kdeconnect #is there an sfos app yet?
        keepassxc
        krename-qt5
        libreoffice
        lm_sensors
        lolcat
        minitube #i don't even use it
        mpv
        ncdu
        nextcloud-client
        nix-index #i can search for files in nix packages... coool
        nix-zsh-completions #better install them twice
        notepadqq #hurrdurr emacs masterrace
        oxygen #pretty hard to breathe without
        oxygen-icons5
        pavucontrol
        pciutils #why the fuck is lspci included in here but not in base?
        pcsx2 #wanna heat up my notebook?
        powertop
        psensor
        qbittorrent
        qsyncthingtray
        quasselClient
        qownnotes
        quaternion #qool matrix client
        quota
        smplayer
        syncthing
        thunderbird
        tigervnc
        toilet
        unrar
        unzip
        vimHugeX
        vlc
        vscode #powershell indent crashes on windows
        weechat
        x11vnc
        xorg.xev #pressing keys n fun
        xorg.xmodmap
        xrdp
        yakuake #i should configure it already
        youtube-dl
      ];
  };
powerManagement.enable = true;
powerManagement.powertop.enable = true;
system.copySystemConfiguration = true;
system.stateVersion = "18.09";
}
