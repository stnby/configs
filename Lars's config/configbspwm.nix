{ config, pkgs, lib, ... }:
{
  imports =
    [
    /etc/nixos/hardware-configuration.nix
    (import (builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz ) {}).nixos
  ];
  nixpkgs.config.packageOverrides = pkgs: { 
      comptonkawase = pkgs.compton.overrideAttrs (oldAttrs: {
        src = pkgs.fetchFromGitLab{
          owner = "dorian7k7";
          repo = "compton-kawase-blur";
          rev = "4b9ac1e83fd783ea2dfa68dfc5f00da99722e12d";
          sha256 = "0xc2hmhpa6zbw09xmhwacqhlgqfdc0g6d4b06r5k56c3ngafsyp0";
          };
      });
      chromiumVAAPI = pkgs.chromium.override {
        VAAPISupport = true;
        };
  };
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true;
  nixpkgs.config.android_sdk.accept_license = true;
  boot = {
    tmpOnTmpfs = true;
    initrd.kernelModules = [ "nouveau" "i915" ];
    initrd.luks.devices = [
      {
        name = "root";
        device = "/dev/disk/by-uuid/a24d974c-6281-49f9-9b4b-9bba76808813";
        preLVM = true;
        allowDiscards = true;
      }
    ];
    plymouth.enable = true;
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = [ "ntfs" ];
    extraModprobeConfig =  ''
      options thinkpad_acpi fan_control=1
      options thinkpad_acpi experimental=1
      options i915 fastboot=1
      options i915 enable_fbc=1
      options i915 lvds_downclock=1
      options i915 modeset=0
    '';
    loader = {
      efi.canTouchEfiVariables = true;
      grub.enable = true;
      grub.version = 2;
      grub.device = "nodev";
      grub.efiSupport = true;
    };
    kernelModules = [ "acpi_call" "tp-smapi" "kvm-intel"];
    extraModulePackages = [ 
      config.boot.kernelPackages.tp_smapi 
      config.boot.kernelPackages.acpi_call];
      kernelParams = [ "intel_iommu=on" "net.ifnames=0" "biosdevname=0" "acpi_osi=Linux" "nmi_watchdog=0" "drm.vblankoffdelay=1" "snd-hda-intel.power_save=1" "iwlwifi.led_mode=2" "iwlwifi.power_save=Y" "iwlwifi.11n_disable=1" "e1000e.SmartPowerDownEnable=1" "quiet" "loglevel=3" "vga=current"];
  };
  virtualisation = {
    libvirtd =  {
      enable = true;
      extraConfig = ''
            unix_sock_group = "libvirtd"
            nix_sock_rw_perms = "0770"'';
      qemuOvmf = true;
      qemuVerbatimConfig = ''user = "1000" '';
    };
  };
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "de";
    defaultLocale = "en_US.UTF-8";
  };
  time.timeZone = "Europe/Berlin";
  environment.systemPackages = with pkgs;  [
    gnome3.dconf
    acpi
    tpacpi-bat
    tlp
    zsh-completions
    nix-zsh-completions
    mumble_git
    libsForQt511.qtstyleplugin-kvantum
  ];
  networking = {
    hostName = "NixLarf";
    networkmanager.enable = true;
    firewall.enable = false;
  };
  hardware = {
    cpu.intel.updateMicrocode = true;
    opengl = {
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntel
        libva-full
        ];
    };
    pulseaudio.enable = true;
    pulseaudio.support32Bit = true;
    bluetooth.enable = true;
  };
  sound.enable = true;
  sound.mediaKeys.enable = true;
  services = {
    tlp = {
      enable = true;
         extraConfig = ''
            SATA_LINKPWR_ON_AC=max_performance
            SATA_LINKPWR_ON_BAT=min_power
            WIFI_PWR_ON_AC=off
            WIFI_PWR_ON_BAT=on
            CPU_SCALING_GOVERNOR_ON_AC=performace
            CPU_SCALING_GOVERNOR_ON_BAT=powersave
            DEVICES_TO_ENABLE_ON_STARTUP="wifi"
            DEVICES_TO_DISABLE_ON_STARTUP="bluetooth wwan"
            RESTORE_DEVICE_STATE_ON_STARTUP=0
            RESTORE_THRESHOLDS_ON_BAT=0
        '';
      };
    flatpak.enable = true;
    fstrim.enable = true;
    thermald.enable = true;
    printing.enable = true;
    colord.enable = true;
    samba.enable = true; 
    xserver = {
      enable =true;
      layout = "de";
      libinput.enable = true;
       displayManager.sessionCommands = ''
        xrandr --setprovideroutputsource 1 0
        xmodmap /home/lars/.Xmodmap
        '';
      displayManager.sddm.enable = true;
      desktopManager = {
        xterm.enable = false;
#        plasma5 = {
#          enable = true;
#          enableQt4Support = true;
#        };
      };
      windowManager.bspwm.enable = true;
      windowManager.bspwm.configFile = "/home/lars/.config/bspwm/bspwmrc";
      windowManager.bspwm.sxhkd.configFile = "/home/lars/.config/sxhkd/sxhkdrc";
    };
  };
  systemd.services.guix-daemon = {
  enable = true;
  description = "Build daemon for GNU Guix";
  serviceConfig = {
    ExecStart = "/var/guix/profiles/per-user/root/guix-profile/bin/guix-daemon --build-users-group=guixbuild";
    Environment="GUIX_LOCPATH=/root/.guix-profile/lib/locale";
    RemainAfterExit="yes";
    StandardOutput="syslog";
    StandardError="syslog";
    TaskMax= "8192";
  };
  wantedBy = [ "multi-user.target" ];
};
  systemd.services.autoGc = {
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-collect-garbage --delete-older-than 14d";
    };
  };
  systemd.timers.autoGc = {
    enable = true;
    timerConfig = {
      Unit = "autoGc.service";
      OnCalendar = "02:00";
      Persistent = "true";
    };
    wantedBy = [ "timers.target" ];
  };
  systemd.services.autoUpgrade = {
    after = [ "network-online.target" "autoGc.service" ];
    path = [ pkgs.gnutar pkgs.xz.bin config.nix.package.out ];
    environment = config.nix.envVars //
    { inherit (config.environment.sessionVariables) NIX_PATH;
    HOME = "/root";
  } // config.networking.proxy.envVars;

    serviceConfig = {
      ExecStart = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch --upgrade";
    };
    wantedBy= [ "autoGc.service" "autoUpgrade.service" ];
  };
  systemd.services.autoOptimise = {
    after = [ "network-online.target" ];
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-store --optimise";
    };
    wantedBy = [ "autoUpgrade.service" ];
  };
  systemd.extraConfig = "DefaultLimitNOFILE=1048576";
  programs.adb.enable = true;
  programs.light.enable = true;
  users.extraUsers = {
    lars = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/lars";
      extraGroups = [ "wheel" "networkmanager" "libvirtd" "video" "adbusers" "kvm" ];
      shell = pkgs.zsh;
    };
    guixbuilder1 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder2 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder3 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder4 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder5 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder6 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder7 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder8 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    };
  environment.pathsToLink = [ "/share/zsh" ];
  environment.extraInit = ''
    export _JAVA_AWT_WM_NONREPARENTING=1
    export XDG_CURRENT_DESKTOP=GNOME
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export QT_AUTO_SCREEN_SCALE_FACTOR=0
    export TERM=rxvt
  '';
  nix.nixPath = ["nixpkgs=\"${fetchTarball http://nixos.org/channels/nixos-unstable/nixexprs.tar.xz}\"" "nixos-config=/home/lars/configuration.nix" "/nix/var/nix/profiles/per-user/root/channels" ];
  home-manager.users.lars =
    let
      master = import (builtins.fetchTarball { url = https://github.com/NixOS/nixpkgs/archive/master.tar.gz; }) {};
      stable = import (builtins.fetchTarball { url = https://github.com/NixOS/nixpkgs/archive/release-18.09.tar.gz; }) {};
    in {
      home.file = {
        ".config/bspwm/bspwmrc".executable = true;
        ".config/bspwm/bspwmrc".text = ''
          #!/bin/sh
          export _JAVA_AWT_WM_NONREPARENTING=1
          export XDG_CURRENT_DESKTOP=GNOME
          export QT_QPA_PLATFORMTHEME="qt5ct"
          export QT_AUTO_SCREEN_SCALE_FACTOR=0
          export TERM=rxvt
          setxkbmap de
          bspc config focus_follows_pointer true
          bspc monitor -d 1 2 3 4 5 6 7 8 9 0
          bspc config border_width	1	
          bspc config window_gap		4
          bspc config split_ratio 	0.52
          bspc config borderless_monocle	true
          bspc config gapless_monocle	true
          feh --bg-scale /home/lars/.local/62233.png
          QSyncthingTray &
          nm-applet &
          pa-applet &
          xmodmap ~/.Xmodmap &
          telegram-desktop -startintray &
          keepassxc &
          '';
        ".config/sxhkd/sxhkdrc".executable = true;
        ".config/sxhkd/sxhkdrc".text = ''
# screen-locker
XF86Launch1
	i3lock-fancy
# brightness
XF86MonBrightnessUp
  light -A 10
XF86MonBrightnessDown
  light -U 10
# toggle terminal
super + Return
	alacritty

# program launchers
super + @space
	rofi -show drun

alt + @space
	dmenu_run

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

#
# bspwm hotkeys
#

# quit an application
super + alt + q
	bspc node -c

# kill an application

super +alt + shift + q
	bspc node -k
# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
super + g
	bspc node -s biggest

#
# state/flags
#

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local
alt + Tab
	bspc node -f next.local
alt + shift + Tab
	bspc node -f prev.local

# focus the next/previous desktop in the current monitor
super + alt + {a,s}
	bspc desktop -f {prev,next}

# focus the last node/desktop
super + {grave,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1,2,3,4,5,6,7,8,9}
	bspc {desktop -f,node -d} '^{1,2,3,4,5,6,7,8,9}'

super + {_,shift + } +0
	bspc {desktop -f,node -d} '^10'
#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}
        '';
        ".config/alacritty/alacritty.yml".executable = true;
        ".config/alacritty/alacritty.yml".text = ''window:
  dimensions:
    columns: 0
    lines: 0
  padding:
    x: 2
    y: 2
  dynamic_padding: false
  decorations: full
  start_maximized: false
scrolling:
  history: 10000
  multiplier: 3
  faux_multiplier: 3
  auto_scroll: false
tabspaces: 8
font:
  size: 11.0
  offset:
    x: 0
    y: 0
  glyph_offset:
    x: 0
    y: 0
  use_thin_strokes: true
render_timer: false
persistent_logging: false
draw_bold_text_with_bright_colors: true
colors:
  primary:
    background: '0x000000'
    foreground: '0xeaeaea'
  normal:
    black:   '0x000000'
    red:     '0xd54e53'
    green:   '0xb9ca4a'
    yellow:  '0xe6c547'
    blue:    '0x7aa6da'
    magenta: '0xc397d8'
    cyan:    '0x70c0ba'
    white:   '0xeaeaea'
  bright:
    black:   '0x666666'
    red:     '0xff3334'
    green:   '0x9ec400'
    yellow:  '0xe7c547'
    blue:    '0x7aa6da'
    magenta: '0xb77ee0'
    cyan:    '0x54ced6'
    white:   '0xffffff'
  indexed_colors: []
visual_bell:
  animation: EaseOutExpo
  duration: 0
  color: '0xffffff'
background_opacity: 0.4
mouse_bindings:
  - { mouse: Middle, action: PasteSelection }
mouse:
  double_click: { threshold: 300 }
  triple_click: { threshold: 300 }
  hide_when_typing: false
  url:
    modifiers: None
selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>"
  save_to_clipboard: false
dynamic_title: true
cursor:
  style: Block
  unfocused_hollow: true
live_config_reload: true
enable_experimental_conpty_backend: false
alt_send_esc: true
key_bindings:
  - { key: Paste,                   action: Paste                            }
  - { key: Copy,                    action: Copy                             }
  - { key: L,        mods: Control, action: ClearLogNotice                   }
  - { key: L,        mods: Control, chars: "\x0c"                            }
  - { key: Home,                    chars: "\x1bOH",        mode: AppCursor  }
  - { key: Home,                    chars: "\x1b[H",        mode: ~AppCursor }
  - { key: End,                     chars: "\x1bOF",        mode: AppCursor  }
  - { key: End,                     chars: "\x1b[F",        mode: ~AppCursor }
  - { key: PageUp,   mods: Shift,   action: ScrollPageUp,   mode: ~Alt       }
  - { key: PageUp,   mods: Shift,   chars: "\x1b[5;2~",     mode: Alt        }
  - { key: PageUp,   mods: Control, chars: "\x1b[5;5~"                       }
  - { key: PageUp,                  chars: "\x1b[5~"                         }
  - { key: PageDown, mods: Shift,   action: ScrollPageDown, mode: ~Alt       }
  - { key: PageDown, mods: Shift,   chars: "\x1b[6;2~",     mode: Alt        }
  - { key: PageDown, mods: Control, chars: "\x1b[6;5~"                       }
  - { key: PageDown,                chars: "\x1b[6~"                         }
  - { key: Tab,      mods: Shift,   chars: "\x1b[Z"                          }
  - { key: Back,                    chars: "\x7f"                            }
  - { key: Back,     mods: Alt,     chars: "\x1b\x7f"                        }
  - { key: Insert,                  chars: "\x1b[2~"                         }
  - { key: Delete,                  chars: "\x1b[3~"                         }
  - { key: Left,     mods: Shift,   chars: "\x1b[1;2D"                       }
  - { key: Left,     mods: Control, chars: "\x1b[1;5D"                       }
  - { key: Left,     mods: Alt,     chars: "\x1b[1;3D"                       }
  - { key: Left,                    chars: "\x1b[D",        mode: ~AppCursor }
  - { key: Left,                    chars: "\x1bOD",        mode: AppCursor  }
  - { key: Right,    mods: Shift,   chars: "\x1b[1;2C"                       }
  - { key: Right,    mods: Control, chars: "\x1b[1;5C"                       }
  - { key: Right,    mods: Alt,     chars: "\x1b[1;3C"                       }
  - { key: Right,                   chars: "\x1b[C",        mode: ~AppCursor }
  - { key: Right,                   chars: "\x1bOC",        mode: AppCursor  }
  - { key: Up,       mods: Shift,   chars: "\x1b[1;2A"                       }
  - { key: Up,       mods: Alt,     chars: "\x1b[1;3A"                       }
  - { key: Up,                      chars: "\x1b[A",        mode: ~AppCursor }
  - { key: Up,                      chars: "\x1bOA",        mode: AppCursor  }
  - { key: Down,     mods: Shift,   chars: "\x1b[1;2B"                       }
  - { key: Down,     mods: Alt,     chars: "\x1b[1;3B"                       }
  - { key: Down,                    chars: "\x1b[B",        mode: ~AppCursor }
  - { key: Down,                    chars: "\x1bOB",        mode: AppCursor  }
  - { key: F1,                      chars: "\x1bOP"                          }
  - { key: F2,                      chars: "\x1bOQ"                          }
  - { key: F3,                      chars: "\x1bOR"                          }
  - { key: F4,                      chars: "\x1bOS"                          }
  - { key: F5,                      chars: "\x1b[15~"                        }
  - { key: F6,                      chars: "\x1b[17~"                        }
  - { key: F7,                      chars: "\x1b[18~"                        }
  - { key: F8,                      chars: "\x1b[19~"                        }
  - { key: F9,                      chars: "\x1b[20~"                        }
  - { key: F10,                     chars: "\x1b[21~"                        }
  - { key: F11,                     chars: "\x1b[23~"                        }
  - { key: F12,                     chars: "\x1b[24~"                        }
  - { key: F1,       mods: Shift,   chars: "\x1b[1;2P"                       }
  - { key: F2,       mods: Shift,   chars: "\x1b[1;2Q"                       }
  - { key: F3,       mods: Shift,   chars: "\x1b[1;2R"                       }
  - { key: F4,       mods: Shift,   chars: "\x1b[1;2S"                       }
  - { key: F5,       mods: Shift,   chars: "\x1b[15;2~"                      }
  - { key: F6,       mods: Shift,   chars: "\x1b[17;2~"                      }
  - { key: F7,       mods: Shift,   chars: "\x1b[18;2~"                      }
  - { key: F8,       mods: Shift,   chars: "\x1b[19;2~"                      }
  - { key: F9,       mods: Shift,   chars: "\x1b[20;2~"                      }
  - { key: F10,      mods: Shift,   chars: "\x1b[21;2~"                      }
  - { key: F11,      mods: Shift,   chars: "\x1b[23;2~"                      }
  - { key: F12,      mods: Shift,   chars: "\x1b[24;2~"                      }
  - { key: F1,       mods: Control, chars: "\x1b[1;5P"                       }
  - { key: F2,       mods: Control, chars: "\x1b[1;5Q"                       }
  - { key: F3,       mods: Control, chars: "\x1b[1;5R"                       }
  - { key: F4,       mods: Control, chars: "\x1b[1;5S"                       }
  - { key: F5,       mods: Control, chars: "\x1b[15;5~"                      }
  - { key: F6,       mods: Control, chars: "\x1b[17;5~"                      }
  - { key: F7,       mods: Control, chars: "\x1b[18;5~"                      }
  - { key: F8,       mods: Control, chars: "\x1b[19;5~"                      }
  - { key: F9,       mods: Control, chars: "\x1b[20;5~"                      }
  - { key: F10,      mods: Control, chars: "\x1b[21;5~"                      }
  - { key: F11,      mods: Control, chars: "\x1b[23;5~"                      }
  - { key: F12,      mods: Control, chars: "\x1b[24;5~"                      }
  - { key: F1,       mods: Alt,     chars: "\x1b[1;6P"                       }
  - { key: F2,       mods: Alt,     chars: "\x1b[1;6Q"                       }
  - { key: F3,       mods: Alt,     chars: "\x1b[1;6R"                       }
  - { key: F4,       mods: Alt,     chars: "\x1b[1;6S"                       }
  - { key: F5,       mods: Alt,     chars: "\x1b[15;6~"                      }
  - { key: F6,       mods: Alt,     chars: "\x1b[17;6~"                      }
  - { key: F7,       mods: Alt,     chars: "\x1b[18;6~"                      }
  - { key: F8,       mods: Alt,     chars: "\x1b[19;6~"                      }
  - { key: F9,       mods: Alt,     chars: "\x1b[20;6~"                      }
  - { key: F10,      mods: Alt,     chars: "\x1b[21;6~"                      }
  - { key: F11,      mods: Alt,     chars: "\x1b[23;6~"                      }
  - { key: F12,      mods: Alt,     chars: "\x1b[24;6~"                      }
  - { key: F1,       mods: Super,   chars: "\x1b[1;3P"                       }
  - { key: F2,       mods: Super,   chars: "\x1b[1;3Q"                       }
  - { key: F3,       mods: Super,   chars: "\x1b[1;3R"                       }
  - { key: F4,       mods: Super,   chars: "\x1b[1;3S"                       }
  - { key: F5,       mods: Super,   chars: "\x1b[15;3~"                      }
  - { key: F6,       mods: Super,   chars: "\x1b[17;3~"                      }
  - { key: F7,       mods: Super,   chars: "\x1b[18;3~"                      }
  - { key: F8,       mods: Super,   chars: "\x1b[19;3~"                      }
  - { key: F9,       mods: Super,   chars: "\x1b[20;3~"                      }
  - { key: F10,      mods: Super,   chars: "\x1b[21;3~"                      }
  - { key: F11,      mods: Super,   chars: "\x1b[23;3~"                      }
  - { key: F12,      mods: Super,   chars: "\x1b[24;3~"                      }
  - { key: NumpadEnter,             chars: "\n"                              }
  - { key: Up,       mods: Control, action: IncreaseFontSize                 }
  - { key: Down,     mods: Control, action: DecreaseFontSize                 }
        '';
        ".Xdefaults".executable =true;
        ".Xdefaults".text = ''
          *foreground: #00cc00
          *foreground_bold: #a8a19f
          *cursor: #a8a19f
          *background:       #1b1918
          !!*background: rgba(0, 0, 0, .7)
          
          !! black
          *color0: #1b1918
          *color8: #766e6b
          !! red
          *color1: #f22c40
          *color9: #f22c40
          !! green
          *color2: #5ab738
          *color10: #5ab738
          !! yellow
          *color3: #d5911a
          *color11: #d5911a
          !! blue
          *color4: #407ee7
          *color12: #407ee7
          !! magenta
          *color5: #6666ea
          *color13: #6666ea
          !! cyan
          *color6: #00ad9c
          *color14: #00ad9c
          !! white
          *color7: #a8a19f
          *color15: #f1efee
          
          rofi.color-enabled:	true
          rofi.color-window:	#000, #000, #000
          rofi.color-normal:	#111, #819396, #222, #008ed4, #ffffff
          rofi.color-active:	#002b37, #008ed4, #003643, #008ed4, #66c6ff
          rofi.color-urgent:	#002b37, #da4281, #003643, #008ed4, #890661
          
          rofi.fake-transparency:	true
          rofi.lines:		3
          rofi.bw:		0
          rofi.opacity:		"10"
          rofi.hide-scrollbar:	true
          rofi.width:		30
          
          Xcursor.theme: Maya-Blue
        '';
        ".Xmodmap".executable = true;
        ".Xmodmap".text = ''
          remove Lock = Caps_Lock
          keysym Caps_Lock = Escape
        '';
        };
      programs.zsh.enable = true;
      programs.zsh.history.size = 1000000;
      programs.zsh.oh-my-zsh.enable = true;
      programs.zsh.oh-my-zsh.plugins = ["history" "git" "cp" "systemd" "colorize" "command-not-found"];
      programs.zsh.plugins = [
        {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-autosuggestions";
          rev = "v0.5.0";
          sha256 = "19qkg4b2flvnp2l0cbkl4qbrnl8d3lym2mmh1mx9nmjd7b81r3pf";
          };
        }
        {
        name = "zsh-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-syntax-highlighting";
          rev = "v0.6.0";
          sha256 = "0zmq66dzasmr5pwribyh4kbkk23jxbpdw4rjxx0i7dx8jjp2lzl4";
          };
        }
      ];
      programs.zsh.oh-my-zsh.theme = "agnoster";
      programs.zsh.shellAliases = {
        nup = "env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
        nupc = "sudo nix-collect-garbage -d && sudo nix-store --optimise && env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";};
      programs.zsh.initExtra = "zstyle \":completion:*:commands\" rehash 1
      source ~/.profile";
      services = {
        dunst.enable = true;
        flameshot.enable = true;
        network-manager-applet.enable = true;
        nextcloud-client.enable = true;
        pasystray.enable = true;
        compton = {
          enable = true;
          package = pkgs.comptonkawase;
          extraOptions = ''
            glx-no-stencil = true;
            glx-copy-from-front = false;
            glx-swap-method = "undefined";
             
            shadow = true;
            no-dnd-shadow = true;
            no-dock-shadow = true;
            # clear-shadow = true;
            shadow-radius = 5;
            shadow-offset-x = -5;
            shadow-offset-y = -5;
            shadow-opacity = 0.5;
            shadow-ignore-shaped = false;
            frame-opacity = 1;
            inactive-opacity-override = false;
            alpha-step = 0.06;
             
            blur-method = "kawase";
            blur-strength = 10;
            blur-background = true;
            blur-background-frame = true;
             
            mark-wmwin-focused = true;
            mark-ovredir-focused = true;
            use-ewmh-active-win = true;
            detect-rounded-corners = true;
            detect-client-opacity = true;
             
             
            dbe = false;
            paint-on-overlay = true;
            sw-opti = true;
            unredir-if-possible = true;
            focus-exclude = [ ];
            detect-transient = true;
            detect-client-leader = true;
            wintypes :
            {
                tooltip :
                  {
                        fade = true;
                        shadow = false;
                        opacity = 0.85;
                        focus = true;
                  };
            };
          '';
        };
        polybar.enable = true;
        polybar.script = "polybar top &";
        polybar.config = {
          "colors" = {
            background = "#00000000";
            background-alt = "#444";
            foreground = "#003256";
            foreground-alt = "#21840d";
            primary = "#ff0000";
            secondary = "#ff0c00";
            alert = "#bd2c40";
            col = "#00f";
          };
          "bar/top" = {
            width = "100%";
            height = "24";
            fixed-center = "false";
            background = "\${colors.background}";
            foreground = "\${colors.foreground}";
            line-size = "3";
            line-color = "{$xrdb:color1}";
            border-size = "0";
            border-color = "#00000000";
            padding-left = "0";
            padding-right = "0";
            module-margin-left = "1";
            module-margin-right = "2";
            font-0 = "mono:pixelsize=9;1";
            modules-center = "bspwm";
            modules-right = "cpu memory filesystem wlan eth battery temperature date";
            tray-position = "right";
            tray-padding = "0 ";
          };
          "module/filesystem" = {
            type = "internal/fs";
            interval = "25";
            mount-0 = "/";
            label-mounted = "%mountpoint%: %used%/%total%: %percentage_used%%";
            label-unmounted = "%mountpoint% not mounted";
            label-unmounted-foreground = "\${colors.foreground-alt}";
          };
          "module/bspwm" = {
            wm-restack = "bspwm";
            scroll-up = "bspwm-desknext";
            scroll-down = "bspwm-deskprev";
            type = "internal/bspwm";
            label-focused = "%index%";
            label-focused-background = "\${colors.background-alt}";
            label-focused-underline= "\${colors.primary}";
            label-focused-padding = "2";
            label-occupied = "%index%";
            label-occupied-padding = "2";
            label-urgent = "%index%!";
            label-urgent-background = "\${colors.alert}";
            label-urgent-padding = "2";
            label-empty = "%index%";
            label-empty-foreground = "\${colors.foreground-alt}";
            label-empty-padding = "2";
          };
          "module/cpu" = {
            type = "internal/cpu";
            interval = "2";
            format-prefix = "CPU: ";
            label = "%percentage%%";
          };
          "module/memory" = {
            type = "internal/memory";
            interval = "2";
            format-prefix = "MEM: ";
            label = "%percentage_used%%";
          };
          "module/wlan" = {
            type = "internal/network";
            interface = "wlan0";
            interval = "3.0";
            format-connected = "<label-connected>";
            format-connected-underline = "\${colors.primary}";
            label-connected = "%essid%";
            format-disconnected = "<label-disconnected>";
            format-disconnected-underline = "\${colors.foreground}";
            label-disconnected = "no wifi";
            label-disconnected-foreground = "\${colors.foreground-alt}";
          };
          "module/eth" = {
            type = "internal/network";
            interface = "eth0";
            interval = "3.0";
            format-connected-underline = "\${colors.primary}";
            format-connected-prefix-foreground = "\${colors.primary}";
            label-connected = "%local_ip%";
            format-disconnected = "<label-disconnected>";
            format-disconnected-underline = "\${colors.foreground}";
            label-disconnected = "no eth";
            label-disconnected-foreground = "\${colors.foreground-alt}";
          };
          "module/date" = {
            type = "internal/date";
            interval = "1  ";
            date = "%A, the %d.%m(%B).%Y";
            time = ", %X";
            format-underline = "\${colors.foreground-alt}";
            label = "%date% %time%";
          };
          "module/battery" = {
            type = "internal/battery";
            battery = "BAT0";
            adapter = "ADP1";
            full-at = "98";
            format-charging = "<label-charging>";
            format-charging-underline = "\${colors.primary}";
            format-discharging = "<label-discharging>";
            format-discharging-underline = "\${colors.foreground}";
            format-full-prefix = "Full ";
            format-full-prefix-foreground = "\${colors.foreground-alt}";
            format-full-underline = "\${colors.foreground-alt}";
          };
          "module/temperature" = {
            type = "internal/temperature";
            thermal-zone = "0";
            warn-temperature = "60";
            format = "<label>";
            format-underline = "\${colors.foreground-alt}";
            format-warn = "<label-warn>";
            format-warn-underline = "\${colors.primary}";
            label = "%temperature-c%";
            label-warn = "%temperature-c%";
            label-warn-foreground = "\${colors.secondary}";
          };
          "settings" = {
            screenchange-reload = "true";
          };
          "global/wm" = {
            margin-top = "5";
            margin-bottom = "5";
          };
        };
        screen-locker = {
          enable = true;
          lockCmd = "i3lock-fancy";
        };
      };
      nixpkgs.config.allowUnfree = true;
      home.packages = with pkgs; [
        master.tdesktop
        stable.stuntrally
        stable.vdrift
        alacritty
        anydesk
        arc-kde-theme
        ark
        chromium
        clementine
        cmatrix
        dmenu
        evince
        feh
        ffmpeg-full
        firefox
        freerdp
        gimp-with-plugins
        git
        glibcLocales
        glxinfo
        gnome3.networkmanagerapplet
        htop
        i3lock-fancy
        jdk
        kdeconnect
        keepassxc
        krename-qt5
        ktorrent
        libreoffice
        lm_sensors
        lolcat
        minitube
        mpv
        ncdu
        nix-index
        nix-zsh-completions
        notepadqq
        oxygen
        oxygen-icons5
        pa_applet
        pavucontrol
        pciutils
        pcsx2
        phototonic
        polybar
        powertop
        psensor
        qbittorrent
        qsyncthingtray
        qt5ct
        qtox
        quasselClient
        quaternion
        quota
        rofi
        rofi-systemd
        smplayer
        sxhkd
        syncthing
        teamspeak_client
        thunderbird
        tigervnc
        toilet
        unrar
        unzip
        vimHugeX
        virtmanager
        vlc
        vscode
        weechat
        wineFull
        winetricks
        x11vnc
        xclip
        xorg.xev
        xorg.xmodmap
        xrdp
        yakuake
        youtube-dl
      ];
  };
powerManagement.enable = true;
powerManagement.powertop.enable = true;
system.copySystemConfiguration = true;
system.stateVersion = "18.09";
}
